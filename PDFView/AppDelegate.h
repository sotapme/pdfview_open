// AppDelegate.h
#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet PDFView *pdfView;
@end
