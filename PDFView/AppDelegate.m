// AppDelegate.m
#import "AppDelegate.h"
@implementation AppDelegate
@synthesize window = _window;
@synthesize pdfView = _pdfView;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:@"TestPage" withExtension:@"pdf"];
    PDFDocument *document = [[PDFDocument alloc] initWithURL: pdfURL];     
    self.pdfView.document = document;
}
@end
