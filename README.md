
Code associated with a problem posted on [Stackoverflow](http://stackoverflow.com/questions/11197627/cocoa-pdfview-only-displays-pdf-on-resize) about a PDFView not showing it's content until resized.

A solution was to create an empty sub-class of PDFView and use that as a CustomView.

The code at a3947158984b shows the original error.
